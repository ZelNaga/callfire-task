package com.asv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallfireListenerApp {
    public static void main(String[] args) {
        SpringApplication.run(CallfireListenerApp.class, args);
    }
}
