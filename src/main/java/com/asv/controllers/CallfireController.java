package com.asv.controllers;

import com.google.common.io.CharStreams;
import lombok.extern.log4j.Log4j;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@RestController
@Log4j
public class CallfireController {
    @RequestMapping(value = "/callback", method = RequestMethod.POST)
    public void callbackListener(HttpServletRequest req) {
        try {
            String targetString = CharStreams.toString(req.getReader());
            InputStream stream = new ByteArrayInputStream(targetString.getBytes());
            SAXBuilder saxBuilder = new SAXBuilder();
            Document document = saxBuilder.build(stream);

            XMLOutputter xmlOutput = new XMLOutputter();
            xmlOutput.setFormat(Format.getPrettyFormat());
            log.info("Payload " + xmlOutput.outputString(document));
        } catch (IOException | JDOMException e) {
            log.error("Something goes wrong", e);
        }
    }
}
