package com.asv.config;

import com.asv.components.CallFireApiCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CallfireConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public CallFireApiCredentials callFireApiCredentials() {
        return new CallFireApiCredentials();
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean(CallFireApiCredentials.class)
    public UsernamePasswordCredentials usernamePasswordCredentials(CallFireApiCredentials apiCredentials) {
        return new UsernamePasswordCredentials(apiCredentials.getCallfireLogin(), apiCredentials.getCallfirePassword());
    }
}
