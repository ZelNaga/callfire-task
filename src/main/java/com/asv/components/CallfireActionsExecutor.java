package com.asv.components;

import lombok.extern.log4j.Log4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Component
@Log4j
public class CallfireActionsExecutor implements ApplicationListener<ApplicationReadyEvent> {
    private final CallFireApiCredentials apiCredentials;
    private final UsernamePasswordCredentials passwordCredentials;

    public CallfireActionsExecutor(CallFireApiCredentials apiCredentials, UsernamePasswordCredentials passwordCredentials) {
        this.apiCredentials = apiCredentials;

        this.passwordCredentials = passwordCredentials;
    }

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {

        try (CloseableHttpClient client = HttpClients.createDefault()) {

            String broadcastId = createBroadcastAndGetItId(client);
            subscribeToEvent(broadcastId, client);
            sendMessage(broadcastId, client);

        } catch (IOException | AuthenticationException | JDOMException e) {

            e.printStackTrace();
        }
    }

    private String createBroadcastAndGetItId(CloseableHttpClient client) throws AuthenticationException, IOException, JDOMException {

        HttpPost httpPost = new HttpPost(apiCredentials.getCallfireApiUrl().concat("broadcast"));
        httpPost.addHeader(new BasicScheme().authenticate(passwordCredentials, httpPost, null));

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("Message", apiCredentials.getMessage()));
        params.add(new BasicNameValuePair("Name", "ASV Broadcast"));
        params.add(new BasicNameValuePair("Type", "TEXT"));
        params.add(new BasicNameValuePair("ToNumber", apiCredentials.getReceiverPhoneNumber()));
        httpPost.setEntity(new UrlEncodedFormEntity(params));

        CloseableHttpResponse response = client.execute(httpPost);

        SAXBuilder saxBuilder = new SAXBuilder();
        Document document = saxBuilder.build(response.getEntity().getContent());

        return document.getRootElement()
                .getChild("Id", Namespace.getNamespace("http://api.callfire.com/resource"))
                .getText();
    }

    private void subscribeToEvent(String broadcastId, CloseableHttpClient client) throws AuthenticationException, IOException {

        HttpPost httpPost = new HttpPost(apiCredentials.getCallfireApiUrl().concat("subscription"));
        httpPost.addHeader(new BasicScheme().authenticate(passwordCredentials, httpPost, null));

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("Enabled", "true"));
        params.add(new BasicNameValuePair("Endpoint", apiCredentials.getCallfireCallbackUrl()));
        params.add(new BasicNameValuePair("TriggerEvent", "OUTBOUND_TEXT_FINISHED"));
        params.add(new BasicNameValuePair("ToNumber", apiCredentials.getReceiverPhoneNumber()));
        params.add(new BasicNameValuePair("FromNumber", apiCredentials.getSenderPhoneNumber()));
        params.add(new BasicNameValuePair("BroadcastId", broadcastId));

        httpPost.setEntity(new UrlEncodedFormEntity(params));
        logHttpResponse("subscribeToEvent", client.execute(httpPost));
    }

    private void sendMessage(String broadcastId, CloseableHttpClient client) throws AuthenticationException, IOException {

        HttpPost httpPost = new HttpPost(apiCredentials.getCallfireApiUrl().concat("text"));
        httpPost.addHeader(new BasicScheme().authenticate(passwordCredentials, httpPost, null));

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("Message", apiCredentials.getMessage()));
        params.add(new BasicNameValuePair("To", apiCredentials.getReceiverPhoneNumber()));
        params.add(new BasicNameValuePair("BroadcastId", broadcastId));

        httpPost.setEntity(new UrlEncodedFormEntity(params));
        logHttpResponse("sendMessage", client.execute(httpPost));
    }

    private void logHttpResponse(String methodName, CloseableHttpResponse response) throws IOException {

        log.info("Method name " + methodName);
        log.info("Response status " + response.getStatusLine().getStatusCode());
        String responseContent = IOUtils.toString(response.getEntity().getContent(), Charset.defaultCharset());
        log.info("Response content " + responseContent);
    }
}
