package com.asv.components;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "api.credentials")
public class CallFireApiCredentials {
    private String callfireLogin;
    private String callfirePassword;
    private String callfireApiUrl;
    private String callfireCallbackUrl;
    private String receiverPhoneNumber;
    private String senderPhoneNumber;
    private String message;
}
